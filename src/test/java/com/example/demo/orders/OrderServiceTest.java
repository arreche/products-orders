package com.example.demo.orders;

import com.example.demo.products.Product;
import com.example.demo.products.ProductRepository;
import com.example.demo.products.ProductService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductService productService;

    @Test
    void placeOrder() {
        var product = new Product("Sample", BigDecimal.valueOf(0.4));
        product.setId(1L);
        var orderRequest = new OrderRequest("buyer@example.com", List.of(1L));
        var expectedLine = new OrderLine(product);
        var expectedOrder = new Order("buyer@example.com", List.of(expectedLine));

        when(orderRepository.save(any())).thenReturn(expectedOrder);
        when(productService.retrieveProducts(any())).thenReturn(List.of(product));

        var order = orderService.placeOrder(orderRequest);

        assertThat(order).isNotNull();
        assertThat(order.getBuyerEmail()).isEqualTo("buyer@example.com");
        assertThat(order.getOrderLines()).isEqualTo(List.of(expectedLine));
        assertThat(order.getPlacedTime()).isEqualTo(LocalDate.now()); // TODO: Find a reliable way to check date
        assertThat(order.getTotalValue()).isEqualTo(BigDecimal.valueOf(0.4));
    }

    @Test
    void retrieveOrders() {
        var product = new Product("Sample", BigDecimal.valueOf(0.4));
        var expectedLines = new OrderLine(product);
        var expectedOrder = new Order("buyer@example.com", List.of(expectedLines));

        when(orderRepository.findAll()).thenReturn(List.of(expectedOrder));

        var orders = orderService.retrieveOrders();

        assertThat(orders).isNotNull();
        assertThat(orders).hasAtLeastOneElementOfType(Order.class);
        assertThat(orders).containsExactly(expectedOrder);
    }

    // TODO: Test retrieving orders between dates
    // TODO: Test updating price in products will not change orders
    // TODO: Test total value of orders with multiple products
}
