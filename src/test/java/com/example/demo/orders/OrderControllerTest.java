package com.example.demo.orders;

import com.example.demo.products.Product;
import com.example.demo.products.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private OrderService orderService;

    @MockBean
    private ProductService productService;

    @Test
    void postOrder() throws Exception {
        var product = new Product("Sample", BigDecimal.valueOf(2.97));
        product.setId(1L);
        given(productService.retrieveProducts(List.of(1L))).willReturn(List.of(product));

        var orderLine = new OrderLine(product);
        var order = new Order("buyer@example.com", List.of(orderLine));
        order.setId(1L);

        var request = new OrderRequest("buyer@example.com", List.of(1L));
        given(this.orderService.placeOrder(request)).willReturn(order);

        this.mvc.perform(post("/orders")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.buyerEmail", is("buyer@example.com")))
                .andExpect(jsonPath("$.placedTime", is(LocalDate.now().toString())))
                .andExpect(jsonPath("$.totalValue", is(2.97)))
                .andExpect(jsonPath("$.orderLines", hasSize(1)))
                .andExpect(jsonPath("$.orderLines[0].productId", is(1)))
                .andExpect(jsonPath("$.orderLines[0].productName", is("Sample")))
                .andExpect(jsonPath("$.orderLines[0].price", is(2.97)))
                .andExpect(jsonPath("$.orderLines[0].quantity", is(1)));
    }

    @Test
    void getOrders() throws Exception {
        var product = new Product("Sample", BigDecimal.valueOf(1.25));
        product.setId(1L);
        var orderLines = new OrderLine(product);
        var order = new Order("buyer@example.com", List.of(orderLines));
        order.setId(1L);

        given(this.orderService.retrieveOrders()).willReturn(List.of(order));

        this.mvc.perform(get("/orders")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].buyerEmail", is("buyer@example.com")))
                .andExpect(jsonPath("$[0].placedTime", is(LocalDate.now().toString())))
                .andExpect(jsonPath("$[0].totalValue", is(1.25)))
                .andExpect(jsonPath("$[0].orderLines", hasSize(1)))
                .andExpect(jsonPath("$[0].orderLines[0].productId", is(1)))
                .andExpect(jsonPath("$[0].orderLines[0].productName", is("Sample")))
                .andExpect(jsonPath("$[0].orderLines[0].price", is(1.25)))
                .andExpect(jsonPath("$[0].orderLines[0].quantity", is(1)));
    }

    // TODO: Test responses on error cases.
    // - Empty orders
    // - Duplicated product
    // - Constraint violations (name, mail, price,...)
}
