package com.example.demo.products;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductService productService;

    @Test
    void createProduct() {
        var productRequest = new ProductRequest("Sample", BigDecimal.valueOf(0.4));
        var expectedProduct = new Product(productRequest.getName(), productRequest.getPrice());

        when(productRepository.save(any())).thenReturn(expectedProduct);

        var product = productService.createProduct(productRequest);

        assertThat(product).isNotNull();
        assertThat(product.getName()).isEqualTo("Sample");
        assertThat(product.getPrice()).isEqualTo(BigDecimal.valueOf(0.4));
    }

    @Test
    void listProducts() {
        var expectedProduct = new Product("Sample", BigDecimal.valueOf(0.4));

        when(productRepository.findAll()).thenReturn(List.of(expectedProduct));

        var products = productService.listProducts();

        assertThat(products).isNotNull();
        assertThat(products).hasAtLeastOneElementOfType(Product.class);
        assertThat(products).containsExactly(expectedProduct);
    }

    @Test
    void updateProduct() {
        var expectedProduct = new Product("Update", BigDecimal.valueOf(1.99));
        expectedProduct.setId(1L);

        when(productRepository.save(any())).thenReturn(expectedProduct);

        var product = productService.updateProduct(expectedProduct.getId(), expectedProduct);

        assertThat(product).isNotNull();
        assertThat(product.getName()).isEqualTo("Update");
        assertThat(product.getPrice()).isEqualTo("1.99");
    }
}
