package com.example.demo.products;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ProductService productService;

    @Test
    void postProduct() throws Exception {
        var request = new ProductRequest("Sample", BigDecimal.valueOf(0.4));
        var product = new Product(request.getName(), request.getPrice());
        product.setId(1L);

        given(this.productService.createProduct(request)).willReturn(product);

        this.mvc.perform(post("/products")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.name", is("Sample")))
                .andExpect(jsonPath("$.price", is(0.4)));
    }

    @Test
    void getProducts() throws Exception {
        var product = new Product("Sample", BigDecimal.valueOf(0.4));
        product.setId(1L);

        given(this.productService.listProducts()).willReturn(List.of(product));

        this.mvc.perform(get("/products")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].name", is("Sample")))
                .andExpect(jsonPath("$[0].price", is(0.4)));
    }

    @Test
    void putProduct() throws Exception {
        var product = new Product("Updated", BigDecimal.valueOf(9.99));
        product.setId(1L);

        given(this.productService.updateProduct(product.getId(), product)).willReturn(product);

        this.mvc.perform(put("/products/{id}", product.getId())
                .content(objectMapper.writeValueAsString(product))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.name", is("Updated")))
                .andExpect(jsonPath("$.price", is(9.99)));
    }

    // TODO: Test error codes
}
