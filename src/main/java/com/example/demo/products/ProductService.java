package com.example.demo.products;

import com.example.demo.core.BusinessRuleException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public Product createProduct(ProductRequest request) {
        var product = new Product(request.getName(), request.getPrice());

        if (productRepository.exists(Example.of(product)))
            throw new BusinessRuleException("Duplicated Product");

        return productRepository.save(product);
    }

    public List<Product> listProducts() {
        return productRepository.findAll();
    }

    public Product updateProduct(Long id, Product product) {
        product.setId(id);
        return productRepository.save(product);
    }

    public List<Product> retrieveProducts(Iterable<Long> ids) {
        return productRepository.findAllById(ids);
    }
}
