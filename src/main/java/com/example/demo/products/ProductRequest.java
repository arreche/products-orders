package com.example.demo.products;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class ProductRequest {
    @NotBlank
    private String name;
    @PositiveOrZero
    private BigDecimal price;
}
