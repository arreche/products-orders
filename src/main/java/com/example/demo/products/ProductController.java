package com.example.demo.products;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Tag(name = "product", description = "the product API")
public class ProductController {

    private final ProductService productService;

    @Operation(summary = "Create a new product", tags = { "product" })
    @PostMapping(value = "/products", consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Product postProduct(@Valid @RequestBody ProductRequest request) {
        return productService.createProduct(request);
    }

    @Operation(summary = "Get a list of all products", tags = { "product" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product listed"),
    })
    @GetMapping(value = "/products", produces = "application/json")
    public Iterable<Product> getProducts() {
        return productService.listProducts();
    }

    @Operation(summary = "Update a product", tags = { "product" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product updated"),
    })
    @PutMapping(value = "/products/{id}", produces = "application/json")
    public Product putProduct(@PathVariable Long id, @Valid @RequestBody Product product) {
        return productService.updateProduct(id, product);
    }
}
