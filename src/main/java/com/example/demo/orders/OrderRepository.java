package com.example.demo.orders;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByPlacedTimeBefore(Optional<LocalDate> toDate);
    List<Order> findByPlacedTimeAfter(Optional<LocalDate> fromDate);
    List<Order> findByPlacedTimeBetween(Optional<LocalDate> fromDate, Optional<LocalDate> toDate);
}
