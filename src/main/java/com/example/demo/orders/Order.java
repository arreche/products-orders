package com.example.demo.orders;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.Assert;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.FutureOrPresent;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "t_order")
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Order {
    @Id
    @GeneratedValue
    private Long id; // TODO: Use a value object with an UUID

    @Email
    private String buyerEmail;

    @FutureOrPresent
    private LocalDate placedTime = LocalDate.now(); // TODO: Store in UTC so it can be localized by clients

    @ElementCollection
    private List<OrderLine> orderLines = new ArrayList<>();

    private BigDecimal totalValue = BigDecimal.ZERO;

    public Order(String buyerEmail, List<OrderLine> orderLines) {
        this.buyerEmail = buyerEmail;
        this.orderLines = orderLines;

        Assert.notEmpty(orderLines, "Cannot place empty orders");

        this.totalValue = orderLines
                .stream()
                .map(OrderLine::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

}
