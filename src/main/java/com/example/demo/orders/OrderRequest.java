package com.example.demo.orders;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class OrderRequest {
    @Email
    private String buyerEmail;
    @NotEmpty
    private List<Long> productIds = new ArrayList<>();
}
