package com.example.demo.orders;

import com.example.demo.products.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final ProductService productService;

    public Order placeOrder(OrderRequest request) {
        var products = productService.retrieveProducts(request.getProductIds());

        Assert.notEmpty(products, "Cannot place orders without products");

        var orderLines = products.stream().map(OrderLine::new).collect(Collectors.toList());
        var order = new Order(request.getBuyerEmail(), orderLines);
        return orderRepository.save(order);
    }

    public List<Order> retrieveOrdersBetweenDates(Optional<LocalDate> fromDate, Optional<LocalDate> toDate) {
        return orderRepository.findByPlacedTimeBetween(fromDate, toDate);
    }

    public List<Order> retrieveOrdersAfterDate(Optional<LocalDate> fromDate) {
        return orderRepository.findByPlacedTimeAfter(fromDate);
    }

    public List<Order> retrieveOrdersBeforeDate(Optional<LocalDate> toDate) {
        return orderRepository.findByPlacedTimeBefore(toDate);
    }

    public List<Order> retrieveOrders() {
        return orderRepository.findAll();
    }
}
