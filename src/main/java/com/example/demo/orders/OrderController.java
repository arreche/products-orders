package com.example.demo.orders;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Optional;

@Tag(name = "order", description = "the order API")
@RestController
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @Operation(summary = "Placing an order", tags = { "order" })
    @PostMapping(value = "/orders", consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Order postOrder(@Valid @RequestBody OrderRequest request) {
        return orderService.placeOrder(request);
    }

    @Operation(summary = "Retrieving all orders within a given time period", tags = { "order" })
    @GetMapping(value = "/orders", produces = "application/json")
    public Iterable<Order> getOrders(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> fromDate,
                                     @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> toDate) {
        // TODO: Reimplement using the Specification pattern. https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#specifications
        if (fromDate.isPresent() && toDate.isPresent())
            return orderService.retrieveOrdersBetweenDates(fromDate, toDate);
        else if (fromDate.isPresent() && !toDate.isPresent())
            return orderService.retrieveOrdersAfterDate(fromDate);
        else if (!fromDate.isPresent() && toDate.isPresent())
            return orderService.retrieveOrdersBeforeDate(toDate);
        else
            return orderService.retrieveOrders();
    }
}
