package com.example.demo.orders;

import com.example.demo.products.Product;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Embeddable
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderLine {
    @NotNull
    private Long productId; // TODO: Use a value object with an UUID

    @NotBlank
    private String productName;

    @PositiveOrZero
    private BigDecimal price;

    @Min(1)
    private Integer quantity;

    public OrderLine(Product product) {
        this.productId = product.getId();
        this.productName = product.getName();
        this.price = product.getPrice();
        this.quantity = 1; // TODO: Take quantity into account
    }
}
