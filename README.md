A REST web service in Java using Spring Boot with an API that supports basic operations with products and orders

## Tasks

- [ x ] Create a new product.
- [ x ] Get a list of all products.
- [ x ] Update a product.
- [ x ] Placing an order.
- [ x ] Retrieving all orders within a given time period.
- [ x ] Provide unit tests.
- [ x ] Document your REST-API.
- [ x ] Provide a storage solution for persisting the web service’s state.
- [ x ] Have a way to run the service with its dependencies locally.
- [ x ] Propose a protocol/method to add authentication.
- [ x ] Propose a way to make the service redundant.

## Requirements

- Install [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/)

## Quick Start

1. Run the database and API services by executing `./run.sh`
2. Interact with the [API](http://localhost:8080/swagger-ui.html)
3. Stop everything `docker-compose down -v`

## Contributing

1. Run the database via Docker Compose `docker-compose up db`
2. Import and run the project in your ide

## Answers

### Authentication

Two simple and stateless solutions that work well with REST APIs are the Basic Authentication Scheme provided by the
spring-boot-starter-security or a custom middleware/filter to validate JWT tokens issued by an external service.

Given the lack of information about what to protect and how I would stick with the simplest solution that allows
collecting more requirements to design the right solution for the right problem.

### Redundancy

Here is a possible plan to provide the application with redundancy and scalability in a progressive way

0. Setup a self-managed DNS and SSL certificate to change the infra without disrupting the business.  e.g. amazon route 53
1. Run the database in an external node or cluster to separate state from computation e.g. Amazon Aurora.
2. Set up a load balancer in front of the application to support adding more replicas of the service. e.g., Amazon ALB
3. Deploy the service in multiple nodes via an orchestrator to ease adding and operating with replicas e.g. Amazon ECS
4. Consider splitting Product and Order services depending on their predicted usage.
