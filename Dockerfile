# Dockerfile with multi-stage builds and smaller images than openjdk:11-jdk-slim and openjdk:11-jre-slim

# Install dependencies
FROM adoptopenjdk/openjdk11:alpine-slim AS deps
WORKDIR /app
COPY ./pom.xml ./pom.xml
COPY ./.mvn ./.mvn
COPY ./mvnw ./mvnw
RUN ./mvnw dependency:go-offline -B

# Build fat jar
FROM adoptopenjdk/openjdk11:alpine-slim AS build
WORKDIR /app
COPY ./pom.xml ./pom.xml
COPY ./.mvn ./.mvn
COPY ./mvnw ./mvnw
COPY ./src ./src
COPY --from=deps /root/.m2 /root/.m2
RUN ./mvnw package -DskipTests=true

# TODO: Use an smaller image with jre only
# Build image
FROM adoptopenjdk/openjdk11:alpine-jre
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
WORKDIR /app
COPY --from=build /app/target/*.jar ./app.jar
ENTRYPOINT java $JAVA_OPTS -jar ./app.jar
