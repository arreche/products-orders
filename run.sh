#!/bin/bash

function check {
  while ! curl -o /dev/null -s "$1:$2"
  do
    printf '.'
    sleep 1;
  done
}

docker-compose up -d db
check localhost 3306
docker-compose up api
